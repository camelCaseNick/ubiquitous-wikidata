import $ from 'jquery';
import L from 'leaflet';

var map: L.Map = null;
var mappedElements = {};

function mapElements(elements: any) {
	console.debug(elements);

	var groupedElements = {};
	elements.results.bindings.forEach(function(value) {
		groupedElements[value.item.value] = groupedElements[value.item.value] || {
			item: value.item,
			itemLabel: value.itemLabel,
			location: value.location,
			osmObject: []
		};
		if(value.osmObject != undefined) {
			groupedElements[value.item.value].osmObject.push(value.osmObject);
		}
	});
	console.debug(groupedElements);

	Object.keys(groupedElements).forEach(function(key) {
		if ( mappedElements[key] == undefined ) {
			var value = groupedElements[key];
			var location = value.location.value.match(/Point\(([^ ]+) ([^ ]+)\)/);
			location = {
				lat: location[2],
				long: location[1]
			};
			var marker = L.circleMarker([location.lat, location.long], {
				color: value.osmObject.length > 0 ? "green" : "#3388ff",
				radius: 6
			}).addTo(map);
			marker.bindPopup(`<h3>${value.itemLabel.value}</h3>
				${`<a href="${value.item.value}" target="_blank">${value.item.value.match(/http:\/\/www\.wikidata\.org\/entity\/(.+)/)[1]}</a>`}
				<h4>OpenStreetMap</h4>
				<ul>
					${value.osmObject.map(osm => {
						var parts = osm.value.match(/https:\/\/www.openstreetmap.org\/(.).+?\/(.*)/)
						return `<li><a href="${osm.value}" target="_blank">${parts[1]}${parts[2]}</a>`
					}).join("")}
				</ul>`);
			marker.on("click", function(e) {
				console.debug(value);
			});
		}
	});

	Object.assign(mappedElements, groupedElements);
}

function nominatim(query: string) {
	$.ajax({
		url: `https://nominatim.openstreetmap.org/search.php?q=Saarland&polygon_geojson=1&viewbox=`,
		data: {
			"format": "jsonv2",
			"q": query,
			"polygon_geojson": "1"
		},
		beforeSend: function() {
			uiLoadingAdd("nominatim", "Loading search results from Nominatim&hellip;");
		}
	}).always(function() {
		uiLoadingRemove("nominatim");
	}).fail(function(jqXHR, textStatus, errorThrown) {
		if (errorThrown !== "abort") {
			uiErrorAdd("nominatim", `Failed to load details from Nominatim: ${errorThrown}`);
			console.warn(jqXHR, textStatus, errorThrown);
		}
	}).done(function(data) {
		console.debug(data);
		$("#geocoding-results").html("");
		data.forEach(function(item: any, index: number) {
			$("#geocoding-results").append(generateItem(item));
		});
	});

	function generateItem(item: any) {
		let $wrapper = $(`<div>
				<a href="#select">
					${item.icon !== undefined && item.icon !== null ? `<img src="${item.icon}" alt="${item.type}" />` : ""}
					${sanitize(item.display_name)}</a>
				(<a href="https://osm.org/${item.osm_type}/${item.osm_id}">OSM</a>)
			</div>`);
		$wrapper.find("a[href='#select']").click(function(e) {
			e.preventDefault();
			map.fitBounds([[item.boundingbox[0], item.boundingbox[2]],
			[item.boundingbox[1], item.boundingbox[3]]]);
			uiPageSet("home");
		});
		return $wrapper;
	}
}

function sanitize(input: string) {
	var tagBody = '(?:[^"\'>]|"[^"]*"|\'[^\']*\')*';
	var tagOrComment = new RegExp(
		'<(?:'
		// Comment body.
		+ '!--(?:(?:-*[^->])*--+|-?)'
		// Special "raw text" elements whose content should be elided.
		+ '|script\\b' + tagBody + '>[\\s\\S]*?</script\\s*'
		+ '|style\\b' + tagBody + '>[\\s\\S]*?</style\\s*'
		// Regular name
		+ '|/?[a-z]'
		+ tagBody
		+ ')>',
		'gi');
	var oldHtml : string;
	do {
		oldHtml = input;
		input = input.replace(tagOrComment, '');
	} while (input !== oldHtml);
	return input.replace(/</g, '&lt;');
}

function setupMap() {
	// Create map element
	map = L.map('map').setView([51, 10], 6);
	L.layerGroup().addTo(map);

	// Add OSM as layer
	L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
		attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
		maxNativeZoom: 19,
		maxZoom: 21
	}).addTo(map);

	// Add scale
	L.control.scale().addTo(map);

	// Move event
	map.on("moveend", function() {
		if (map.getZoom() < 12) {
			$("#request").attr("disabled", "disabled");
		} else {
			$("#request").removeAttr("disabled");
		}
	});

	// Enable hash
	new (<any>L).Hash(map);
	
	// Add custom controls to map
	var customNavControls = function(opts) {
		var CustomNavControls = L.Control.extend({
			onAdd: function(map) {
				// Search control
				var searchControlIcon = createIconRow("Search for a place", "button", "img/search.svg");
				searchControlIcon.onclick = function(e) {
					e.preventDefault()
					var $textfield = $("#leaflet-control-search-input");
					if ($textfield.val() === "" || $textfield.val() === $textfield.attr("placeholder")) {
						$textfield.val($textfield.attr("placeholder"));
					}
					$textfield.focus().select();
				}
				expandableLabelRow(searchControlIcon, searchControlIcon.getAttribute("title"));
				var searchControlField = L.DomUtil.create('input');
				searchControlField.id = "leaflet-control-search-input";
				searchControlField.setAttribute("placeholder", "Look for a place...");
				searchControlField.setAttribute("type", "text");
				searchControlField.onblur = function(e) {
					$(this).closest(".leaflet-control-expandable").removeClass("open");
				}
				searchControlField.onfocus = function(e) {
					$(this).closest(".leaflet-control-expandable").addClass("open");
				}
				searchControlField.onkeyup = function(e) {
					if (e.key == "Enter") {
						uiPageSet("geocoding");
						$("#geocoding-query").val($(this).val()).attr("readonly", "true");
						nominatim(String($("#geocoding-query").val()));
					}
				}
				var searchControlWrapper = L.DomUtil.create('div', 'leaflet-control-expandable-row');
				searchControlWrapper.appendChild(searchControlIcon);
				searchControlWrapper.appendChild(searchControlField);
				var searchControlContainer = L.DomUtil.create('div', 'leaflet-control-growth');
				searchControlContainer.appendChild(searchControlWrapper);
				// Locate control
				var locateControl = createIconRow("Locate", "button", "img/locate.svg");
				locateControl.id = "control_locate";
				locateControl.onclick = function(e) {
					e.preventDefault();
					map.on("locationerror", function(e) {
						console.debug(e);
						$("#control_locate").addClass("blocked");
						uiErrorAdd("location", `Failed to aquire location: ${e.message}`)
					});
					map.on("locationfound", function(e) {
						console.debug(e);
						$("#control_locate").removeClass("blocked");
						uiErrorRemove("location")
					});
					map.locate({
						setView: true
					});
				};
				// Create group
				var control_container = L.DomUtil.create('div', 'leaflet-bar leaflet-control leaflet-control-expandable');
				control_container.appendChild(searchControlWrapper);
				control_container.appendChild(expandableLabelRow(locateControl, locateControl.getAttribute("title")));
				return control_container;
			},
			onRemove: function(map) {
				// Nothing to do here
			}
		});
		return new CustomNavControls(opts);
	}
	var customExtControls = function(opts) {
		var CustomExtControls = L.Control.extend({
			onAdd: function(map) {
				// Osm control
				var osmControl = createIconRow("Open current view in OpenStreetMap", "button", "img/openstreetmap.svg");
				osmControl.onmouseenter = function(e) {
					$(osmControl).attr("href", `https://osm.org/#map=${map.getZoom()}/${map.getCenter().lat}/${map.getCenter().lng}`);
				};
				// Overpass control
				var overpassTurboControl = createIconRow("Open current view in Overpass Turbo", "button", "https://wiki.openstreetmap.org/w/images/c/c3/Overpass-turbo.svg");
				overpassTurboControl.onmouseenter = function(e) {
					$(overpassTurboControl).attr("href", `https://overpass-turbo.eu/?C=${map.getCenter().lat};${map.getCenter().lng};${map.getZoom()}`);
				};
				// JOSM control
				var josmControl = createIconRow("Open current view in JOSM", "button", "https://upload.wikimedia.org/wikipedia/commons/c/c0/JOSM_Logotype_2019.svg");
				josmControl.onclick = function(e) {
					e.preventDefault();

					uiLoadingAdd("josm", "Communicating with JOSM&hellip;");
					$.ajax({
						data: {
							"left": map.getBounds().getWest(),
							"right": map.getBounds().getEast(),
							"top": map.getBounds().getNorth(),
							"bottom": map.getBounds().getSouth()
						},
						type: "GET",
						url: "http://localhost:8111/zoom"
					}).always(function() {
						uiLoadingRemove("josm");
					}).fail(function(jqXHR, textStatus, errorThrown) {
						if (errorThrown !== "abort") {
							uiErrorAdd("josm", `Failed to communicate with JOSM: ${errorThrown}`);
							console.warn(jqXHR, textStatus, errorThrown);
						}
					});
				};
				josmControl.onmouseenter = function(e) {
					$(josmControl).attr("href", `http://localhost:8111/zoom?left=${map.getBounds().getWest()}&right=${map.getBounds().getEast()}&top=${map.getBounds().getNorth()}&bottom=${map.getBounds().getSouth()}`);
				};
				// Create group
				var control_container = L.DomUtil.create('div', 'leaflet-bar leaflet-control leaflet-control-expandable');
				control_container.appendChild(expandableLabelRow(osmControl, osmControl.getAttribute("title")));
				control_container.appendChild(expandableLabelRow(overpassTurboControl, overpassTurboControl.getAttribute("title")));
				control_container.appendChild(expandableLabelRow(josmControl, josmControl.getAttribute("title")));

				return control_container;
			},
			onRemove: function(map) {
				// Nothing to do here
			}
		});
		return new CustomExtControls(opts);
	}
	customNavControls({
		position: "topleft"
	}).addTo(map);
	customExtControls({
		position: "topleft"
	}).addTo(map);

	function createIconRow(title: string, role: string, icon: string) {
		var newItem = L.DomUtil.create('a', "leaflet-control-image");
		newItem.setAttribute("aria-label", title);
		newItem.setAttribute("href", "#");
		newItem.setAttribute("role", role);
		newItem.setAttribute("title", title);
		newItem.innerHTML = `<span style="background-image:url(${icon})"></span>`;
		return newItem;
	}

	function wrapInExpandableRow(left, right) {
		var expandableRow = L.DomUtil.create("div", "leaflet-control-expandable-row");
		expandableRow.appendChild(left);
		expandableRow.appendChild(right);
		return expandableRow;
	}

	function expandableLabelRow(left, label: string) {
		var expandableLabel = L.DomUtil.create("span", "leaflet-control-expandable-label");
		expandableLabel.innerHTML = label;
		return wrapInExpandableRow(left, expandableLabel);
	}
}

function request() {
	// Check, if zoom great enough
	if (map.getZoom() < 12) {
		console.debug("Not zoomed in far enough");
		$("#control_downloadtoggle").addClass("blocked");
		return;
	} else {
		$("#control_downloadtoggle").removeClass("blocked");
	}

	uiLoadingAdd("wdqs", "Loading elements from WDQS&hellip;");
	$("#control_downloadtoggle").addClass("working");
	$("#request").attr("disabled", "disabled");

	// Request
	$.ajax({
		beforeSend: function(request) {
			request.setRequestHeader("Accept", "application/json, text/plain, */*");
		},
		data: {
			"query": `PREFIX osmt: <https://wiki.openstreetmap.org/wiki/Key:>
			SELECT ?item ?itemLabel ?location ?osmObject WHERE {
				SERVICE wikibase:label {
					bd:serviceParam wikibase:language "${$("#settings_wdLabelLanguages").val()},[AUTO_LANGUAGE],en"
				}
				SERVICE wikibase:box {
					?item wdt:P625 ?location .
					bd:serviceParam wikibase:cornerSouthWest "Point(${map.getBounds().getWest()} ${map.getBounds().getSouth()})"^^geo:wktLiteral .
					bd:serviceParam wikibase:cornerNorthEast "Point(${map.getBounds().getEast()} ${map.getBounds().getNorth()})"^^geo:wktLiteral .
				}
				OPTIONAL {
					SERVICE <https://sophox.org/sparql> {
						{
							?osmObject osmt:wikidata ?item.
						} UNION {
							?osmObject osmt:building:wikidata ?item.
						}
					}
				}
			}
			LIMIT ${$("#settings_bindingsLimit").val()}`.replace(/\n/g, " ").replace(/\t/g, "")
		},
		type: "GET",
		url: "https://query.wikidata.org/sparql"
	}).always(function() {
		uiLoadingRemove("wdqs");
		$("#request").removeAttr("disabled");
		$("#control_downloadtoggle").removeClass("working");
	}).fail(function(jqXHR, textStatus, errorThrown) {
		if (errorThrown !== "abort") {
			uiErrorAdd("wdqs", `Failed to load elements from WDQS: ${errorThrown}`);
			console.warn(jqXHR, textStatus, errorThrown);
		}
	}).done(function(data) {
		mapElements(data);
	});
}

function uiErrorAdd(identifier: string, message: string, timeout: number = 10000) {
	if ($(`#error-${identifier}`).length == 0) {
		$("#errormsg-wrapper ul").append(`<li id="error-${identifier}">${message}</li>`);
	} else {
		$(`#error-${identifier}`).html(message);
	}
	if (timeout != null && timeout != 0) {
		setTimeout(function() {
			uiErrorRemove(identifier);
		}, timeout);
	}
	$("#errormsg-wrapper").addClass("visible");
}

function uiErrorRemove(identifier: string): boolean {
	if ($(`#error-${identifier}`).length == 0) {
		return false;
	} else {
		$(`#error-${identifier}`).remove();
		if ($("#errormsg-wrapper ul").children().length == 0) {
			$("#errormsg-wrapper").removeClass("visible");
		}
		return true;
	}
}

function uiLoadingAdd(identifier: string, message: string) {
	if ($(`#loading-${identifier}`).length == 0) {
		$("#item-loadingindicator").append(`<span id="loading-${identifier}">${message}</span>`);
	} else {
		$(`#loading-${identifier}`).html(message);
	}
	$("#item-header").addClass("loading");
}

function uiLoadingRemove(identifier: string) {
	if ($(`#loading-${identifier}`).length == 0) {
		return false;
	} else {
		$(`#loading-${identifier}`).remove();
		if ($("#item-loadingindicator").children().length == 0) {
			$("#item-header").removeClass("loading");
		}
		return true;
	}
}

function uiPageSet(identifier: string) {
	$(".aside-main").find("*[data-page]").removeClass("visible");
	$(".aside-main").find(`*[data-page=${identifier}]`).addClass("visible");
}

function main() {
	setupMap();
	$(".aside-arrowback").click(function(e) {
		e.preventDefault();
		uiPageSet($(this).attr("href").match(/#(.*)/)[1]);
	});
	$("#request").on("click", function(e) {
		e.preventDefault();
		request();
	});
	$("#request").on("mouseenter", function() {
		if (map.getZoom() < 12) {
			uiErrorAdd("zoom", "Please zoom in before downloading data");
		}
	});
}

main();